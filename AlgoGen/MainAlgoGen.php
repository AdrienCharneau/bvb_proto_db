<?php
//Algo Genetique : 2020 Arnaud Chretien
//pas de framework pas de namespace
//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
require('../debug_dd.php');
require('./Classes/Robot.php');
require('./Functions/statRobotManagement.php');
require('./Functions/Conformity.php');
//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

$robots = Genese();
$robots = Validation($robots,"noice");
$robots = Selection($robots);

//Genese : produit une nouvelle génération de robot selon certains critères
function Genese()
{
	//Genere des robots dans une liste de robot
	$generationRobot = array();
	for ($i=0; $i < 25; $i++) 
	{ 
		//$wins, $looses, $stat_hp, $weapon_id, $stat_attack, $stat_speed, $behavior_agility, $behavior_proximity, $behavior_aggressivity
		$statRobot = RandomiseStatRobot(1,100);
		$robot = new Robot( 0, 0, $statRobot['stat_hp'], $statRobot['stat_attack'], $statRobot["stat_speed"], $statRobot["behavior_agility"], $statRobot["behavior_proximity"], $statRobot["behavior_aggressivity"], 1 );	// <- ces stats sont randomisées dans la ligne précendente
		$generationRobot[$i] = $robot;
	}
	return $generationRobot;
}

//Validation : check si un robot est conformes aux exigences de notre génération
function Validation($robots, String $exigence) 
{
	//Vire les doublons et check si la generation à assez de nouveau individus
	$robots = deleteDoublons($robots); // TODO
	if ( checkGenerationConformity($robots) == false )
	{
		destruction();
	}

	//Check si chaque robot à des stats inclues entre 0 et 100
	foreach($robots as $robot)
	{
		if (checkRobotConformity($robot) == false)
			unset($robots[$robot]);
	}
	
	//echo "$exigence";
	return $robots;
	//return newRobots;
}

//Destruction : détruit une generation de robot si elle n'est pas conforme aux exigences de notre génération, puis relance la genèse
function Destruction()
{
	//etape conceptuelle, ici on se contente de relancer une generation
	//quand plus tard on fera varier les parametre d'entrée de la genese, on pourra mettre une déviation ici
	header("Location: http://localhost:8888/bvb_proto_db/AlgoGen/MainAlgoGen.php"); //TODO
    exit;
}

//Selection : lance la bagarre sur unity pour obtenir un vainqueur
function Selection($robots)
{
	//TODO

	$robotsRdyToSend = ConcateneGeneration($robots);
	echo($robotsRdyToSend);
	//on envoie la gen a unity, quand il aura finit il fera un autre appel pour finaliser l'algo
}
