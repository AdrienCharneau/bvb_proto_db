<?php

function RandomiseStatRobot(int $min, int $max)
{
	$statRobot = array(
		'weapon_id' => 0 ,
		'stat_hp' => 0 ,
		'stat_attack' => 0 ,
		'stat_speed' => 0 ,
		'behavior_agility' => 0 ,
		'behavior_proximity' => 0 ,
		'behavior_aggressivity' => 0 ,
	 );
	foreach ($statRobot as $key => $value) 
	{
		$value = random_int ($min , $max ) ;
		$statRobot[$key] = $value;
	}
	//dd($statRobot);
	return $statRobot;
}

//transforme un robot en un string
function ConcateneStatRobot($robot)
{
	$concated = "";
	foreach ($robot as $stat) 
	{
		if (empty($stat) == false) {
			$concated = $concated."\t".$stat;
		}
		
	}
	return substr($concated, 1);
}

//transforme une generation de robot en un string
function ConcateneGeneration($gen)
{
	$concated = "";
	foreach ($gen as $robot) 
	{
		$concated = $concated."#".ConcateneStatRobot($robot);
	}
	return substr($concated, 1);
}

function deConcateneGeneration($gen)
{
	$robotsRdy = [];

	$robotsTab = explode("#", $gen);
	foreach ($robotsTab as $robot) 
	{
		$robot = deConcateneRobot($robot);
		$robotsRdy[] = $robot;
	}
	return $robotsRdy;
}
function deConcateneRobot($robot)
{
	$robotStat = explode(" ", $robot);
	$robot = new Robot( $robotStat[0], $robotStat[1], $robotStat[2], $robotStat[3], $robotStat[4], $robotStat[5], $robotStat[6], $robotStat[7], $robotStat[8] );
	return $robot;
}