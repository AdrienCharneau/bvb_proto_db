<?php
require('../model.php');

    function CheckRobotConformity($robot)
    {
        foreach($robot as $statRobot)
        {
            if ($statRobot < 0 || $statRobot > 100)
                return false;
        }
        return true;
    }

    function CheckGenerationConformity($gen)
    {
        $NumberOfExistingRobot = 0;
        //check le % de nouveau robot dans cette generation, si > x , on delete et re-genese
        foreach ($gen as $robot)
        {
            $test =  GetRobotFromStats( $robot->stat_hp, $robot->stat_attack , $robot->stat_speed, $robot->behavior_agility, $robot->behavior_proximity, $robot->behavior_aggressivity,  $robot->weapon_id);
            //si la db ne retourne rien, le robot n'existe pas
            if (empty($test) == false)
                $NumberOfExistingRobot++;
        }
        //si plus de 50 robots existe déja, la generation n'est pas considéré comme pertinente
        if ($NumberOfExistingRobot > 50)
        {
            return false;
        }
        else
            return true;
    }

    function DeleteDoublons($robots)
    {
        //$final_generation = deleteDoublons($robots) ; // TODO
        return $robots;
    }

    function CheckIfRobotExist($robot){

        $bddRobot = GetRobotFromStats( $robot->stat_hp, $robot->stat_attack , $robot->stat_speed, $robot->behavior_agility, $robot->behavior_proximity, $robot->behavior_aggressivity,  $robot->weapon_id);
        if (empty($bddRobot) == true)
            return false;
        else
            return $bddRobot;

    }

